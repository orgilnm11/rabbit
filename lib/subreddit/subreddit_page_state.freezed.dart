// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'subreddit_page_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SubredditPageStateTearOff {
  const _$SubredditPageStateTearOff();

  _SubredditPageStateLoading loading() {
    return const _SubredditPageStateLoading();
  }

  _SubredditPageStateSuccess success(
      {@required Subreddit subreddit, @required List<Link> links}) {
    return _SubredditPageStateSuccess(
      subreddit: subreddit,
      links: links,
    );
  }
}

// ignore: unused_element
const $SubredditPageState = _$SubredditPageStateTearOff();

mixin _$SubredditPageState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result success(Subreddit subreddit, List<Link> links),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result success(Subreddit subreddit, List<Link> links),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(_SubredditPageStateLoading value),
    @required Result success(_SubredditPageStateSuccess value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(_SubredditPageStateLoading value),
    Result success(_SubredditPageStateSuccess value),
    @required Result orElse(),
  });
}

abstract class $SubredditPageStateCopyWith<$Res> {
  factory $SubredditPageStateCopyWith(
          SubredditPageState value, $Res Function(SubredditPageState) then) =
      _$SubredditPageStateCopyWithImpl<$Res>;
}

class _$SubredditPageStateCopyWithImpl<$Res>
    implements $SubredditPageStateCopyWith<$Res> {
  _$SubredditPageStateCopyWithImpl(this._value, this._then);

  final SubredditPageState _value;
  // ignore: unused_field
  final $Res Function(SubredditPageState) _then;
}

abstract class _$SubredditPageStateLoadingCopyWith<$Res> {
  factory _$SubredditPageStateLoadingCopyWith(_SubredditPageStateLoading value,
          $Res Function(_SubredditPageStateLoading) then) =
      __$SubredditPageStateLoadingCopyWithImpl<$Res>;
}

class __$SubredditPageStateLoadingCopyWithImpl<$Res>
    extends _$SubredditPageStateCopyWithImpl<$Res>
    implements _$SubredditPageStateLoadingCopyWith<$Res> {
  __$SubredditPageStateLoadingCopyWithImpl(_SubredditPageStateLoading _value,
      $Res Function(_SubredditPageStateLoading) _then)
      : super(_value, (v) => _then(v as _SubredditPageStateLoading));

  @override
  _SubredditPageStateLoading get _value =>
      super._value as _SubredditPageStateLoading;
}

class _$_SubredditPageStateLoading implements _SubredditPageStateLoading {
  const _$_SubredditPageStateLoading();

  @override
  String toString() {
    return 'SubredditPageState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _SubredditPageStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result success(Subreddit subreddit, List<Link> links),
  }) {
    assert(loading != null);
    assert(success != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result success(Subreddit subreddit, List<Link> links),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(_SubredditPageStateLoading value),
    @required Result success(_SubredditPageStateSuccess value),
  }) {
    assert(loading != null);
    assert(success != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(_SubredditPageStateLoading value),
    Result success(_SubredditPageStateSuccess value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _SubredditPageStateLoading implements SubredditPageState {
  const factory _SubredditPageStateLoading() = _$_SubredditPageStateLoading;
}

abstract class _$SubredditPageStateSuccessCopyWith<$Res> {
  factory _$SubredditPageStateSuccessCopyWith(_SubredditPageStateSuccess value,
          $Res Function(_SubredditPageStateSuccess) then) =
      __$SubredditPageStateSuccessCopyWithImpl<$Res>;
  $Res call({Subreddit subreddit, List<Link> links});
}

class __$SubredditPageStateSuccessCopyWithImpl<$Res>
    extends _$SubredditPageStateCopyWithImpl<$Res>
    implements _$SubredditPageStateSuccessCopyWith<$Res> {
  __$SubredditPageStateSuccessCopyWithImpl(_SubredditPageStateSuccess _value,
      $Res Function(_SubredditPageStateSuccess) _then)
      : super(_value, (v) => _then(v as _SubredditPageStateSuccess));

  @override
  _SubredditPageStateSuccess get _value =>
      super._value as _SubredditPageStateSuccess;

  @override
  $Res call({
    Object subreddit = freezed,
    Object links = freezed,
  }) {
    return _then(_SubredditPageStateSuccess(
      subreddit:
          subreddit == freezed ? _value.subreddit : subreddit as Subreddit,
      links: links == freezed ? _value.links : links as List<Link>,
    ));
  }
}

class _$_SubredditPageStateSuccess implements _SubredditPageStateSuccess {
  const _$_SubredditPageStateSuccess(
      {@required this.subreddit, @required this.links})
      : assert(subreddit != null),
        assert(links != null);

  @override
  final Subreddit subreddit;
  @override
  final List<Link> links;

  @override
  String toString() {
    return 'SubredditPageState.success(subreddit: $subreddit, links: $links)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SubredditPageStateSuccess &&
            (identical(other.subreddit, subreddit) ||
                const DeepCollectionEquality()
                    .equals(other.subreddit, subreddit)) &&
            (identical(other.links, links) ||
                const DeepCollectionEquality().equals(other.links, links)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(subreddit) ^
      const DeepCollectionEquality().hash(links);

  @override
  _$SubredditPageStateSuccessCopyWith<_SubredditPageStateSuccess>
      get copyWith =>
          __$SubredditPageStateSuccessCopyWithImpl<_SubredditPageStateSuccess>(
              this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result success(Subreddit subreddit, List<Link> links),
  }) {
    assert(loading != null);
    assert(success != null);
    return success(subreddit, links);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result success(Subreddit subreddit, List<Link> links),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(subreddit, links);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(_SubredditPageStateLoading value),
    @required Result success(_SubredditPageStateSuccess value),
  }) {
    assert(loading != null);
    assert(success != null);
    return success(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(_SubredditPageStateLoading value),
    Result success(_SubredditPageStateSuccess value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _SubredditPageStateSuccess implements SubredditPageState {
  const factory _SubredditPageStateSuccess(
      {@required Subreddit subreddit,
      @required List<Link> links}) = _$_SubredditPageStateSuccess;

  Subreddit get subreddit;
  List<Link> get links;
  _$SubredditPageStateSuccessCopyWith<_SubredditPageStateSuccess> get copyWith;
}
