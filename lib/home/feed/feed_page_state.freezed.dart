// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'feed_page_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$FeedPageStateTearOff {
  const _$FeedPageStateTearOff();

  FeedPageStateLoading loading() {
    return const FeedPageStateLoading();
  }

  FeedPageStateSuccess success({@required List<Link> links}) {
    return FeedPageStateSuccess(
      links: links,
    );
  }

  FeedPageStateGettingMore gettingMore({@required List<Link> links}) {
    return FeedPageStateGettingMore(
      links: links,
    );
  }
}

// ignore: unused_element
const $FeedPageState = _$FeedPageStateTearOff();

mixin _$FeedPageState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result success(List<Link> links),
    @required Result gettingMore(List<Link> links),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result success(List<Link> links),
    Result gettingMore(List<Link> links),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(FeedPageStateLoading value),
    @required Result success(FeedPageStateSuccess value),
    @required Result gettingMore(FeedPageStateGettingMore value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(FeedPageStateLoading value),
    Result success(FeedPageStateSuccess value),
    Result gettingMore(FeedPageStateGettingMore value),
    @required Result orElse(),
  });
}

abstract class $FeedPageStateCopyWith<$Res> {
  factory $FeedPageStateCopyWith(
          FeedPageState value, $Res Function(FeedPageState) then) =
      _$FeedPageStateCopyWithImpl<$Res>;
}

class _$FeedPageStateCopyWithImpl<$Res>
    implements $FeedPageStateCopyWith<$Res> {
  _$FeedPageStateCopyWithImpl(this._value, this._then);

  final FeedPageState _value;
  // ignore: unused_field
  final $Res Function(FeedPageState) _then;
}

abstract class $FeedPageStateLoadingCopyWith<$Res> {
  factory $FeedPageStateLoadingCopyWith(FeedPageStateLoading value,
          $Res Function(FeedPageStateLoading) then) =
      _$FeedPageStateLoadingCopyWithImpl<$Res>;
}

class _$FeedPageStateLoadingCopyWithImpl<$Res>
    extends _$FeedPageStateCopyWithImpl<$Res>
    implements $FeedPageStateLoadingCopyWith<$Res> {
  _$FeedPageStateLoadingCopyWithImpl(
      FeedPageStateLoading _value, $Res Function(FeedPageStateLoading) _then)
      : super(_value, (v) => _then(v as FeedPageStateLoading));

  @override
  FeedPageStateLoading get _value => super._value as FeedPageStateLoading;
}

class _$FeedPageStateLoading implements FeedPageStateLoading {
  const _$FeedPageStateLoading();

  @override
  String toString() {
    return 'FeedPageState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FeedPageStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result success(List<Link> links),
    @required Result gettingMore(List<Link> links),
  }) {
    assert(loading != null);
    assert(success != null);
    assert(gettingMore != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result success(List<Link> links),
    Result gettingMore(List<Link> links),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(FeedPageStateLoading value),
    @required Result success(FeedPageStateSuccess value),
    @required Result gettingMore(FeedPageStateGettingMore value),
  }) {
    assert(loading != null);
    assert(success != null);
    assert(gettingMore != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(FeedPageStateLoading value),
    Result success(FeedPageStateSuccess value),
    Result gettingMore(FeedPageStateGettingMore value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class FeedPageStateLoading implements FeedPageState {
  const factory FeedPageStateLoading() = _$FeedPageStateLoading;
}

abstract class $FeedPageStateSuccessCopyWith<$Res> {
  factory $FeedPageStateSuccessCopyWith(FeedPageStateSuccess value,
          $Res Function(FeedPageStateSuccess) then) =
      _$FeedPageStateSuccessCopyWithImpl<$Res>;
  $Res call({List<Link> links});
}

class _$FeedPageStateSuccessCopyWithImpl<$Res>
    extends _$FeedPageStateCopyWithImpl<$Res>
    implements $FeedPageStateSuccessCopyWith<$Res> {
  _$FeedPageStateSuccessCopyWithImpl(
      FeedPageStateSuccess _value, $Res Function(FeedPageStateSuccess) _then)
      : super(_value, (v) => _then(v as FeedPageStateSuccess));

  @override
  FeedPageStateSuccess get _value => super._value as FeedPageStateSuccess;

  @override
  $Res call({
    Object links = freezed,
  }) {
    return _then(FeedPageStateSuccess(
      links: links == freezed ? _value.links : links as List<Link>,
    ));
  }
}

class _$FeedPageStateSuccess implements FeedPageStateSuccess {
  const _$FeedPageStateSuccess({@required this.links}) : assert(links != null);

  @override
  final List<Link> links;

  @override
  String toString() {
    return 'FeedPageState.success(links: $links)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FeedPageStateSuccess &&
            (identical(other.links, links) ||
                const DeepCollectionEquality().equals(other.links, links)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(links);

  @override
  $FeedPageStateSuccessCopyWith<FeedPageStateSuccess> get copyWith =>
      _$FeedPageStateSuccessCopyWithImpl<FeedPageStateSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result success(List<Link> links),
    @required Result gettingMore(List<Link> links),
  }) {
    assert(loading != null);
    assert(success != null);
    assert(gettingMore != null);
    return success(links);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result success(List<Link> links),
    Result gettingMore(List<Link> links),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(links);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(FeedPageStateLoading value),
    @required Result success(FeedPageStateSuccess value),
    @required Result gettingMore(FeedPageStateGettingMore value),
  }) {
    assert(loading != null);
    assert(success != null);
    assert(gettingMore != null);
    return success(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(FeedPageStateLoading value),
    Result success(FeedPageStateSuccess value),
    Result gettingMore(FeedPageStateGettingMore value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class FeedPageStateSuccess implements FeedPageState {
  const factory FeedPageStateSuccess({@required List<Link> links}) =
      _$FeedPageStateSuccess;

  List<Link> get links;
  $FeedPageStateSuccessCopyWith<FeedPageStateSuccess> get copyWith;
}

abstract class $FeedPageStateGettingMoreCopyWith<$Res> {
  factory $FeedPageStateGettingMoreCopyWith(FeedPageStateGettingMore value,
          $Res Function(FeedPageStateGettingMore) then) =
      _$FeedPageStateGettingMoreCopyWithImpl<$Res>;
  $Res call({List<Link> links});
}

class _$FeedPageStateGettingMoreCopyWithImpl<$Res>
    extends _$FeedPageStateCopyWithImpl<$Res>
    implements $FeedPageStateGettingMoreCopyWith<$Res> {
  _$FeedPageStateGettingMoreCopyWithImpl(FeedPageStateGettingMore _value,
      $Res Function(FeedPageStateGettingMore) _then)
      : super(_value, (v) => _then(v as FeedPageStateGettingMore));

  @override
  FeedPageStateGettingMore get _value =>
      super._value as FeedPageStateGettingMore;

  @override
  $Res call({
    Object links = freezed,
  }) {
    return _then(FeedPageStateGettingMore(
      links: links == freezed ? _value.links : links as List<Link>,
    ));
  }
}

class _$FeedPageStateGettingMore implements FeedPageStateGettingMore {
  const _$FeedPageStateGettingMore({@required this.links})
      : assert(links != null);

  @override
  final List<Link> links;

  @override
  String toString() {
    return 'FeedPageState.gettingMore(links: $links)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FeedPageStateGettingMore &&
            (identical(other.links, links) ||
                const DeepCollectionEquality().equals(other.links, links)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(links);

  @override
  $FeedPageStateGettingMoreCopyWith<FeedPageStateGettingMore> get copyWith =>
      _$FeedPageStateGettingMoreCopyWithImpl<FeedPageStateGettingMore>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result success(List<Link> links),
    @required Result gettingMore(List<Link> links),
  }) {
    assert(loading != null);
    assert(success != null);
    assert(gettingMore != null);
    return gettingMore(links);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result success(List<Link> links),
    Result gettingMore(List<Link> links),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (gettingMore != null) {
      return gettingMore(links);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(FeedPageStateLoading value),
    @required Result success(FeedPageStateSuccess value),
    @required Result gettingMore(FeedPageStateGettingMore value),
  }) {
    assert(loading != null);
    assert(success != null);
    assert(gettingMore != null);
    return gettingMore(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(FeedPageStateLoading value),
    Result success(FeedPageStateSuccess value),
    Result gettingMore(FeedPageStateGettingMore value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (gettingMore != null) {
      return gettingMore(this);
    }
    return orElse();
  }
}

abstract class FeedPageStateGettingMore implements FeedPageState {
  const factory FeedPageStateGettingMore({@required List<Link> links}) =
      _$FeedPageStateGettingMore;

  List<Link> get links;
  $FeedPageStateGettingMoreCopyWith<FeedPageStateGettingMore> get copyWith;
}
