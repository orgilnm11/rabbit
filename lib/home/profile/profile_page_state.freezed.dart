// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'profile_page_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$ProfilePageStateTearOff {
  const _$ProfilePageStateTearOff();

  ProfilePageStateLoading loading() {
    return const ProfilePageStateLoading();
  }

  ProfilePageStateSignedOut signedOut() {
    return const ProfilePageStateSignedOut();
  }

  ProfilePageStateSigningIn signingIn() {
    return const ProfilePageStateSigningIn();
  }

  ProfilePageStateSignedIn signedIn() {
    return const ProfilePageStateSignedIn();
  }

  ProfilePageStateProfile profile({@required User user}) {
    return ProfilePageStateProfile(
      user: user,
    );
  }
}

// ignore: unused_element
const $ProfilePageState = _$ProfilePageStateTearOff();

mixin _$ProfilePageState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result signedOut(),
    @required Result signingIn(),
    @required Result signedIn(),
    @required Result profile(User user),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result signedOut(),
    Result signingIn(),
    Result signedIn(),
    Result profile(User user),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(ProfilePageStateLoading value),
    @required Result signedOut(ProfilePageStateSignedOut value),
    @required Result signingIn(ProfilePageStateSigningIn value),
    @required Result signedIn(ProfilePageStateSignedIn value),
    @required Result profile(ProfilePageStateProfile value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(ProfilePageStateLoading value),
    Result signedOut(ProfilePageStateSignedOut value),
    Result signingIn(ProfilePageStateSigningIn value),
    Result signedIn(ProfilePageStateSignedIn value),
    Result profile(ProfilePageStateProfile value),
    @required Result orElse(),
  });
}

abstract class $ProfilePageStateCopyWith<$Res> {
  factory $ProfilePageStateCopyWith(
          ProfilePageState value, $Res Function(ProfilePageState) then) =
      _$ProfilePageStateCopyWithImpl<$Res>;
}

class _$ProfilePageStateCopyWithImpl<$Res>
    implements $ProfilePageStateCopyWith<$Res> {
  _$ProfilePageStateCopyWithImpl(this._value, this._then);

  final ProfilePageState _value;
  // ignore: unused_field
  final $Res Function(ProfilePageState) _then;
}

abstract class $ProfilePageStateLoadingCopyWith<$Res> {
  factory $ProfilePageStateLoadingCopyWith(ProfilePageStateLoading value,
          $Res Function(ProfilePageStateLoading) then) =
      _$ProfilePageStateLoadingCopyWithImpl<$Res>;
}

class _$ProfilePageStateLoadingCopyWithImpl<$Res>
    extends _$ProfilePageStateCopyWithImpl<$Res>
    implements $ProfilePageStateLoadingCopyWith<$Res> {
  _$ProfilePageStateLoadingCopyWithImpl(ProfilePageStateLoading _value,
      $Res Function(ProfilePageStateLoading) _then)
      : super(_value, (v) => _then(v as ProfilePageStateLoading));

  @override
  ProfilePageStateLoading get _value => super._value as ProfilePageStateLoading;
}

class _$ProfilePageStateLoading implements ProfilePageStateLoading {
  const _$ProfilePageStateLoading();

  @override
  String toString() {
    return 'ProfilePageState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ProfilePageStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result signedOut(),
    @required Result signingIn(),
    @required Result signedIn(),
    @required Result profile(User user),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result signedOut(),
    Result signingIn(),
    Result signedIn(),
    Result profile(User user),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(ProfilePageStateLoading value),
    @required Result signedOut(ProfilePageStateSignedOut value),
    @required Result signingIn(ProfilePageStateSigningIn value),
    @required Result signedIn(ProfilePageStateSignedIn value),
    @required Result profile(ProfilePageStateProfile value),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(ProfilePageStateLoading value),
    Result signedOut(ProfilePageStateSignedOut value),
    Result signingIn(ProfilePageStateSigningIn value),
    Result signedIn(ProfilePageStateSignedIn value),
    Result profile(ProfilePageStateProfile value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class ProfilePageStateLoading implements ProfilePageState {
  const factory ProfilePageStateLoading() = _$ProfilePageStateLoading;
}

abstract class $ProfilePageStateSignedOutCopyWith<$Res> {
  factory $ProfilePageStateSignedOutCopyWith(ProfilePageStateSignedOut value,
          $Res Function(ProfilePageStateSignedOut) then) =
      _$ProfilePageStateSignedOutCopyWithImpl<$Res>;
}

class _$ProfilePageStateSignedOutCopyWithImpl<$Res>
    extends _$ProfilePageStateCopyWithImpl<$Res>
    implements $ProfilePageStateSignedOutCopyWith<$Res> {
  _$ProfilePageStateSignedOutCopyWithImpl(ProfilePageStateSignedOut _value,
      $Res Function(ProfilePageStateSignedOut) _then)
      : super(_value, (v) => _then(v as ProfilePageStateSignedOut));

  @override
  ProfilePageStateSignedOut get _value =>
      super._value as ProfilePageStateSignedOut;
}

class _$ProfilePageStateSignedOut implements ProfilePageStateSignedOut {
  const _$ProfilePageStateSignedOut();

  @override
  String toString() {
    return 'ProfilePageState.signedOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ProfilePageStateSignedOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result signedOut(),
    @required Result signingIn(),
    @required Result signedIn(),
    @required Result profile(User user),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return signedOut();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result signedOut(),
    Result signingIn(),
    Result signedIn(),
    Result profile(User user),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signedOut != null) {
      return signedOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(ProfilePageStateLoading value),
    @required Result signedOut(ProfilePageStateSignedOut value),
    @required Result signingIn(ProfilePageStateSigningIn value),
    @required Result signedIn(ProfilePageStateSignedIn value),
    @required Result profile(ProfilePageStateProfile value),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return signedOut(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(ProfilePageStateLoading value),
    Result signedOut(ProfilePageStateSignedOut value),
    Result signingIn(ProfilePageStateSigningIn value),
    Result signedIn(ProfilePageStateSignedIn value),
    Result profile(ProfilePageStateProfile value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signedOut != null) {
      return signedOut(this);
    }
    return orElse();
  }
}

abstract class ProfilePageStateSignedOut implements ProfilePageState {
  const factory ProfilePageStateSignedOut() = _$ProfilePageStateSignedOut;
}

abstract class $ProfilePageStateSigningInCopyWith<$Res> {
  factory $ProfilePageStateSigningInCopyWith(ProfilePageStateSigningIn value,
          $Res Function(ProfilePageStateSigningIn) then) =
      _$ProfilePageStateSigningInCopyWithImpl<$Res>;
}

class _$ProfilePageStateSigningInCopyWithImpl<$Res>
    extends _$ProfilePageStateCopyWithImpl<$Res>
    implements $ProfilePageStateSigningInCopyWith<$Res> {
  _$ProfilePageStateSigningInCopyWithImpl(ProfilePageStateSigningIn _value,
      $Res Function(ProfilePageStateSigningIn) _then)
      : super(_value, (v) => _then(v as ProfilePageStateSigningIn));

  @override
  ProfilePageStateSigningIn get _value =>
      super._value as ProfilePageStateSigningIn;
}

class _$ProfilePageStateSigningIn implements ProfilePageStateSigningIn {
  const _$ProfilePageStateSigningIn();

  @override
  String toString() {
    return 'ProfilePageState.signingIn()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ProfilePageStateSigningIn);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result signedOut(),
    @required Result signingIn(),
    @required Result signedIn(),
    @required Result profile(User user),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return signingIn();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result signedOut(),
    Result signingIn(),
    Result signedIn(),
    Result profile(User user),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signingIn != null) {
      return signingIn();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(ProfilePageStateLoading value),
    @required Result signedOut(ProfilePageStateSignedOut value),
    @required Result signingIn(ProfilePageStateSigningIn value),
    @required Result signedIn(ProfilePageStateSignedIn value),
    @required Result profile(ProfilePageStateProfile value),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return signingIn(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(ProfilePageStateLoading value),
    Result signedOut(ProfilePageStateSignedOut value),
    Result signingIn(ProfilePageStateSigningIn value),
    Result signedIn(ProfilePageStateSignedIn value),
    Result profile(ProfilePageStateProfile value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signingIn != null) {
      return signingIn(this);
    }
    return orElse();
  }
}

abstract class ProfilePageStateSigningIn implements ProfilePageState {
  const factory ProfilePageStateSigningIn() = _$ProfilePageStateSigningIn;
}

abstract class $ProfilePageStateSignedInCopyWith<$Res> {
  factory $ProfilePageStateSignedInCopyWith(ProfilePageStateSignedIn value,
          $Res Function(ProfilePageStateSignedIn) then) =
      _$ProfilePageStateSignedInCopyWithImpl<$Res>;
}

class _$ProfilePageStateSignedInCopyWithImpl<$Res>
    extends _$ProfilePageStateCopyWithImpl<$Res>
    implements $ProfilePageStateSignedInCopyWith<$Res> {
  _$ProfilePageStateSignedInCopyWithImpl(ProfilePageStateSignedIn _value,
      $Res Function(ProfilePageStateSignedIn) _then)
      : super(_value, (v) => _then(v as ProfilePageStateSignedIn));

  @override
  ProfilePageStateSignedIn get _value =>
      super._value as ProfilePageStateSignedIn;
}

class _$ProfilePageStateSignedIn implements ProfilePageStateSignedIn {
  const _$ProfilePageStateSignedIn();

  @override
  String toString() {
    return 'ProfilePageState.signedIn()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ProfilePageStateSignedIn);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result signedOut(),
    @required Result signingIn(),
    @required Result signedIn(),
    @required Result profile(User user),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return signedIn();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result signedOut(),
    Result signingIn(),
    Result signedIn(),
    Result profile(User user),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signedIn != null) {
      return signedIn();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(ProfilePageStateLoading value),
    @required Result signedOut(ProfilePageStateSignedOut value),
    @required Result signingIn(ProfilePageStateSigningIn value),
    @required Result signedIn(ProfilePageStateSignedIn value),
    @required Result profile(ProfilePageStateProfile value),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return signedIn(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(ProfilePageStateLoading value),
    Result signedOut(ProfilePageStateSignedOut value),
    Result signingIn(ProfilePageStateSigningIn value),
    Result signedIn(ProfilePageStateSignedIn value),
    Result profile(ProfilePageStateProfile value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signedIn != null) {
      return signedIn(this);
    }
    return orElse();
  }
}

abstract class ProfilePageStateSignedIn implements ProfilePageState {
  const factory ProfilePageStateSignedIn() = _$ProfilePageStateSignedIn;
}

abstract class $ProfilePageStateProfileCopyWith<$Res> {
  factory $ProfilePageStateProfileCopyWith(ProfilePageStateProfile value,
          $Res Function(ProfilePageStateProfile) then) =
      _$ProfilePageStateProfileCopyWithImpl<$Res>;
  $Res call({User user});
}

class _$ProfilePageStateProfileCopyWithImpl<$Res>
    extends _$ProfilePageStateCopyWithImpl<$Res>
    implements $ProfilePageStateProfileCopyWith<$Res> {
  _$ProfilePageStateProfileCopyWithImpl(ProfilePageStateProfile _value,
      $Res Function(ProfilePageStateProfile) _then)
      : super(_value, (v) => _then(v as ProfilePageStateProfile));

  @override
  ProfilePageStateProfile get _value => super._value as ProfilePageStateProfile;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(ProfilePageStateProfile(
      user: user == freezed ? _value.user : user as User,
    ));
  }
}

class _$ProfilePageStateProfile implements ProfilePageStateProfile {
  const _$ProfilePageStateProfile({@required this.user}) : assert(user != null);

  @override
  final User user;

  @override
  String toString() {
    return 'ProfilePageState.profile(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ProfilePageStateProfile &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @override
  $ProfilePageStateProfileCopyWith<ProfilePageStateProfile> get copyWith =>
      _$ProfilePageStateProfileCopyWithImpl<ProfilePageStateProfile>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(),
    @required Result signedOut(),
    @required Result signingIn(),
    @required Result signedIn(),
    @required Result profile(User user),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return profile(user);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(),
    Result signedOut(),
    Result signingIn(),
    Result signedIn(),
    Result profile(User user),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (profile != null) {
      return profile(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(ProfilePageStateLoading value),
    @required Result signedOut(ProfilePageStateSignedOut value),
    @required Result signingIn(ProfilePageStateSigningIn value),
    @required Result signedIn(ProfilePageStateSignedIn value),
    @required Result profile(ProfilePageStateProfile value),
  }) {
    assert(loading != null);
    assert(signedOut != null);
    assert(signingIn != null);
    assert(signedIn != null);
    assert(profile != null);
    return profile(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(ProfilePageStateLoading value),
    Result signedOut(ProfilePageStateSignedOut value),
    Result signingIn(ProfilePageStateSigningIn value),
    Result signedIn(ProfilePageStateSignedIn value),
    Result profile(ProfilePageStateProfile value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (profile != null) {
      return profile(this);
    }
    return orElse();
  }
}

abstract class ProfilePageStateProfile implements ProfilePageState {
  const factory ProfilePageStateProfile({@required User user}) =
      _$ProfilePageStateProfile;

  User get user;
  $ProfilePageStateProfileCopyWith<ProfilePageStateProfile> get copyWith;
}
